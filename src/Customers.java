import java.util.Arrays;

public class Customers {
    private Customer[] customers;

    public Customers(Customer[] customers){
        this.customers = customers;
    }

    public void  buyersInAlphabetical(){
        for (int i = 0; i < customers.length; i++) {
            for (int j = i + 1; j < customers.length; j++) {
                if (customers[i].getFirstName().compareTo(customers[j].getSurname()) > 0) {
                    Customer temp = customers[i];
                    customers[i] = customers[j];
                    customers[j] = temp;
                }
            }
        }System.out.println(Arrays.toString(customers));
    }

    public void ifCardInInterval(String from, String to) {
        Customer[] array = new Customer[this.customers.length];
        for (int i = 0; i < array.length; ++i) {
            if (customers[i].getCreditCardNumber().compareTo(from) >= 0 &&
                    customers[i].getCreditCardNumber().compareTo(to) <= 0)
                customers[i].print();
        }
    }
    public String toString(){
        String str = "";
        for (Customer c : customers) {
            str += c.toString() + "; ";
        }
        return str;
    }


    public Customer[] getCustomers() {
        return customers;
    }

    public void setCustomers(Customer[] customers) {
        this.customers = customers;
    }

}

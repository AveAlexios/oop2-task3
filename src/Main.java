public class Main {
    public static void main(String[] args) {

        Customer customerRobert = new Customer(1,"Smith","Robert",
                "Johnson","Minsk","111111","222222");
        Customer customerElizabeth = new Customer(2,"Bathory","Elizabeth",
                "Stefania","Budapest","424242","333333");
        Customer customerLouis = new Customer(3,"Armstrong","Louis",
                " ","New Orleans","222222","4444444");
        Customer customerDiana = new Customer(4,"Warwick","Diana",
                "Winston","New York","555555"," ");


        Customers arrayOfCustomers = new Customers(new Customer[]
                {customerRobert,customerElizabeth,customerLouis,customerDiana});

        arrayOfCustomers.ifCardInInterval("000000","333333");
        arrayOfCustomers.buyersInAlphabetical();

    }
}